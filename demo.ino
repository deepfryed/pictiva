#include "SparkFun_Si7021_Breakout_Library.h"
#include <Wire.h>
#include <SPI.h>
#include <SoftwareSerial.h>
#include "pictiva.h"

Weather si7021;
Pictiva oled(2, 3, 4, 7);

static char buffer[64];
static bool display = true;
static int  ticks = 0;

static void print_labels(void) {
  oled.write8x15(0, 0, 15, "Humidity");
  oled.write8x15(32, 0, 15, "Temperature");
}

static int ftoa(float f, char *buffer, size_t size) {
  int whole = (int)f, fraction = (f - whole) * 100;
  return snprintf(buffer, size, "%d.%02d", whole, fraction);
}

void setup() {
  pinMode(7, OUTPUT);
  digitalWrite(7, HIGH);
  Serial.begin(115200);
  Serial.println("init si7021");
  si7021.begin();
  delay(2000);
  Serial.println("init SPI");
  SPI.begin();
  Serial.println("init oled");
  oled.begin();
  Serial.println("ok");
  print_labels();
}

void loop() {
  if (display) {
    float r_humidity = si7021.getRH();
    float c_temperature = si7021.readTemp();
    ftoa(r_humidity, buffer, sizeof(buffer));
    strcat(buffer, "%");
    oled.write8x15(16, 0, 15, buffer);
    ftoa(c_temperature, buffer, sizeof(buffer));
    strcat(buffer, "C");
    oled.write8x15(48, 0, 15, buffer);
  }

  delay(1000);
  ticks++;
  if (ticks > 5) {
    display = !display;
    ticks = 0;
    if (!display)
      oled.off();
    else {
      oled.on();
      print_labels();
    }
  }
}
