#pragma once

#ifdef __cplusplus
extern "C" {
#endif

class Pictiva {
  public:
    Pictiva(uint8_t cs, uint8_t rst, uint8_t dc, uint8_t vcc);
    void begin(void);
    void close(void);
    void on(void);
    void off(void);

    void setCanvas(uint8_t top, uint8_t left, uint8_t rows, uint8_t cols);

    void clear(void);
    void fill(uint8_t top, uint8_t left, uint8_t rows, uint8_t cols, uint8_t brightness);

    void setPixel(uint8_t row, uint8_t col, uint8_t brightness);

    void write5x7(uint8_t top,uint8_t left, uint8_t brightness, const char* text);
    void write8x15(uint8_t top,uint8_t left, uint8_t brightness, const char* text);
  private:
    uint8_t cs, rs, dc, vcc;

    void write(uint8_t byte);
    void write(const void *data, size_t size);
    void write(uint8_t cmd, uint8_t v1);
    void write(uint8_t cmd, uint8_t v1, uint8_t v2);
    void write(uint8_t cmd, uint8_t v1, uint8_t v2, uint8_t v3);

    void startSPI(bool isData);
    void stopSPI();
};


#ifdef __cplusplus
}
#endif
