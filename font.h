#pragma once

#ifdef __cplusplus
extern "C" {
#endif

extern const char FONT5X7[][5];
extern const char FONT8X15[][8][2];

#ifdef __cplusplus
}
#endif

