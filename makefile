ARDUINO_DIR   = $(HOME)/software/arduino-1.8.5
ARDMK_DIR     = $(HOME)/code/arduino/Arduino-Makefile
AVR_TOOLS_DIR = /usr
AVRDUDE_CONF  = /etc/avrdude.conf
ARCHITECTURE  = avr
MONITOR_PORT ?= /dev/ttyUSB0
BOARD_TAG     = nano
BOARD_SUB     = atmega328
ARDUINO_LIBS  = Wire SPI SoftwareSerial SparkFun_Si7021_Humidity_and_Temperature_Sensor

-include $(ARDMK_DIR)/Arduino.mk

flash: pause upload unpause

pause:
	-@kill -USR1 $$(pgrep -f rserial)
	-@sleep 1

unpause:
	-@kill -USR2 $$(pgrep -f rserial)
