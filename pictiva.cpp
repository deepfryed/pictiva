#include <SPI.h>
#include <Wire.h>
#include "pictiva.h"
#include "font.h"

#define CMD_COLUMN_ADDR                 0x15
#define CMD_ROW_ADDR                    0x75
#define CMD_CONTRAST_REGISTER           0x81
#define CMD_QUARTER_CURRENT_RANGE       0x84
#define CMD_HALF_CURRENT_RANGE          0x85
#define CMD_FULL_CURRENT_RANGE          0x86
#define CMD_REMAPPING                   0xA0
#define CMD_DISPLAY_START_LINE          0xA1
#define CMD_DISPLAY_OFFSET              0xA2
#define CMD_NORMAL_DISPLAY_MODE         0xA4
#define CMD_ENTIRE_DISPLAY_ON           0xA5
#define CMD_ENTIRE_DISPLAY_OFF          0xA6
#define CMD_INVERSE_DISPLAY             0xA7
#define CMD_MULTIPLEX_RATIO             0xA8
#define CMD_DCDC_CONVERTER              0xAD
#define CMD_DISPLAY_OFF                 0xAE
#define CMD_DISPLAY_ON                  0xAF
#define CMD_PHASE_LENGTH                0xB1
#define CMD_ROW_PERIOD                  0xB2
#define CMD_DISPLAY_CLOCK_DIVIDE_RATIO  0xB3
#define CMD_GRAY_SCALE_TABLE            0xB8
#define CMD_PRECHARGE_VOLTAGE           0xBC
#define CMD_VCOMH_VOLTAGE               0xBE
#define CMD_SEGMENT_LOW_VOLTAGE         0xBF
#define CMD_NOP                         0xE3

Pictiva::Pictiva(uint8_t _cs, uint8_t _rs, uint8_t _dc, uint8_t _vcc) {
  cs  = _cs;
  rs  = _rs;
  dc  = _dc;
  vcc = _vcc;

  pinMode(cs,  OUTPUT);
  pinMode(rs,  OUTPUT);
  pinMode(dc,  OUTPUT);
  pinMode(vcc, OUTPUT);
  digitalWrite(vcc, LOW);
}

void Pictiva::begin(void) {
  const uint8_t grayscale_table[] = {0x01,0x11,0x22,0x32,0x43,0x54,0x65,0x76};

  digitalWrite(rs, LOW);
  delayMicroseconds(100);
  digitalWrite(rs, HIGH);

  startSPI(false);
  write(CMD_DISPLAY_OFF);
  digitalWrite(vcc, HIGH);
  delay(100);

  write(CMD_CONTRAST_REGISTER, 0x33);
  write(CMD_FULL_CURRENT_RANGE);
  write(CMD_REMAPPING, 0x41); // 0x52 otherway around.
  write(CMD_DISPLAY_OFFSET, 0x00);
  write(CMD_DISPLAY_START_LINE, 0x00);
  write(CMD_MULTIPLEX_RATIO, 0x3F);
  write(CMD_NORMAL_DISPLAY_MODE);
  write(CMD_PHASE_LENGTH, 0x22);
  write(CMD_ROW_PERIOD, 0x46);
  write(CMD_DISPLAY_CLOCK_DIVIDE_RATIO, 0x41);
  write(CMD_SEGMENT_LOW_VOLTAGE, 0x0D);
  write(CMD_VCOMH_VOLTAGE, 0x00);
  write(CMD_PRECHARGE_VOLTAGE, 0x10);
  write(CMD_GRAY_SCALE_TABLE);
  write(grayscale_table, sizeof(grayscale_table));
  write(CMD_DCDC_CONVERTER, 0x02); // off
  write(CMD_DISPLAY_ON);
  stopSPI();
  clear();
}

void Pictiva::close(void) {
  startSPI(false);
  write(CMD_DISPLAY_OFF);
  digitalWrite(vcc, LOW);
  delay(100);
  stopSPI();
}

void Pictiva::off(void) {
  startSPI(false);
  write(CMD_DISPLAY_OFF);
  stopSPI();
}

void Pictiva::on(void) {
  startSPI(false);
  write(CMD_DISPLAY_ON);
  stopSPI();
}

void Pictiva::clear(void) {
  fill(0, 0, 64, 128, 0x00);
}

void Pictiva::setCanvas(uint8_t top, uint8_t left, uint8_t rows, uint8_t cols) {
  uint8_t x, y, xe, ye;

  x = left >> 1;
  xe = x + (cols >> 1) - 1;

  y = top;
  ye = y + rows - 1;

  if(xe > 63) xe = 63; //clipping
  if(ye > 63) ye = 63; //clipping

  startSPI(false);
  write(CMD_COLUMN_ADDR);
  write(x);
  write(xe); // 0..63  (128/2)

  write(CMD_ROW_ADDR);
  write(y);
  write(ye); // 0..63 (visual) 79 total
  stopSPI();
}

void Pictiva::fill(uint8_t top, uint8_t left, uint8_t rows, uint8_t cols, uint8_t brightness)  {
  // 4bpp, convert 0x00 - 0x0f => levels for 2 pixels.
  brightness = ((brightness << 4) & 0xf0) | (brightness & 0x0f);

  setCanvas(top, left, rows, cols);
  startSPI(true);
  for (uint8_t y = 0; y < rows; y++)
    for (uint8_t x = 0; x < (cols >> 1); x++)
      write(brightness);
  stopSPI();
}


void Pictiva::setPixel(uint8_t row, uint8_t col, uint8_t brightness) {
  setCanvas(row, col, 1, 1);
  startSPI(true);
  write(brightness);
  stopSPI();
}

void Pictiva::write(uint8_t data) {
  SPI.transfer(data);
}

void Pictiva::write(const void *data, size_t size) {
  for (size_t i = 0; i < size; i++)
    SPI.transfer(((uint8_t*)data)[i]);
}

void Pictiva::write(uint8_t cmd, uint8_t v1) {
  SPI.transfer(cmd);
  SPI.transfer(v1);
}

void Pictiva::write(uint8_t cmd, uint8_t v1, uint8_t v2) {
  SPI.transfer(cmd);
  SPI.transfer(v1);
  SPI.transfer(v2);
}

void Pictiva::write(uint8_t cmd, uint8_t v1, uint8_t v2, uint8_t v3) {
  SPI.transfer(cmd);
  SPI.transfer(v1);
  SPI.transfer(v2);
  SPI.transfer(v3);
}

void Pictiva::startSPI(bool isData) {
  digitalWrite(dc, isData ? HIGH : LOW);
  SPI.beginTransaction(SPISettings(8000000, MSBFIRST, SPI_MODE0));
  digitalWrite(cs, LOW);
}

void Pictiva::stopSPI(void) {
  write(CMD_NOP);
  delayMicroseconds(1);
  digitalWrite(cs, HIGH);
  SPI.endTransaction();
}

void Pictiva::write5x7(uint8_t top,uint8_t left, uint8_t brightness, const char* text)  {
  uint8_t b, xstep, ystep, a;

  while (text && *text > 0) {
    a = *text - 32;
    for(xstep = 0; xstep < 3; xstep++) {
      setCanvas(top, left, 8, 2);
      left += 2;
      ystep = 1;
      while(ystep)  {
        b = (brightness & 0x0f) << 4 | (brightness & 0x0f);
        if(!(pgm_read_byte(&(FONT5X7[a][xstep * 2])) & ystep))
          b &= 0x0F;
        if(!((pgm_read_byte(&(FONT5X7[a][xstep * 2 + 1])) & ystep) && (xstep < 2)))
          b &= 0xF0;
        ystep <<= 1;
        startSPI(true);
        write(b);
        stopSPI();
       }
    }
    text++;
  }
}

void Pictiva::write8x15(uint8_t top,uint8_t left, uint8_t brightness, const char* text)  {
  uint16_t a;
  uint8_t b, y, i, xstep, ystep;

  i = 0;
  while (text[i] > 0)   {
    a = text[i] - 32;
    i++;
    for (xstep = 0; xstep < 4; xstep++) {
      setCanvas(top, left, 16, 2);
      y = top;
      left += 2;
      ystep = 0x01;
      while(ystep) {
        b = (brightness & 0x0f) << 4 | (brightness & 0x0f);
        if(!(pgm_read_byte(&(FONT8X15[a][xstep * 2][0])) & ystep))
          b &= 0x0F;

        if(!((pgm_read_byte(&(FONT8X15[a][xstep * 2 + 1][0])) & ystep)))
          b &= 0xF0;
        ystep <<= 1;
        startSPI(true);
        write(b);
        stopSPI();
        y++;
      }

      ystep = 0x01;
      while(ystep) {
        b = (brightness & 0x0f) << 4 | (brightness & 0x0f);
        if(!(pgm_read_byte(&(FONT8X15[a][xstep * 2][1])) & ystep))
          b &= 0x0F;
        if(!((pgm_read_byte(&(FONT8X15[a][xstep * 2 + 1][1])) & ystep)))
          b &= 0xF0;
        ystep <<= 1;
        if(y < 64) {
          startSPI(true);
          write(b);
          stopSPI();
        }
        y++;
      }
    }
  }
}

